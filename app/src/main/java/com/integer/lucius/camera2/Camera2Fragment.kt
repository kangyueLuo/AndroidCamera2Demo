package com.integer.lucius.camera2

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.*
import com.example.android.camera2basic.CompareSizesByArea
import com.integer.lucius.camera2.dialog.ConfirmationDialog
import com.integer.lucius.camera2.dialog.ErrorDialog
import kotlinx.android.synthetic.main.fragment_camera2.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.nio.ByteBuffer
import java.util.*
import java.util.concurrent.*
import kotlin.collections.ArrayList
import kotlin.experimental.and


/**
 * @detail
 *g
 * @author luciuszhang
 * @date 2018/9/19 15:30
 *
 * @modify kangyue
 * @date 2018/11/13 11:46
 * change to Record only
 */
class Camera2Fragment : Fragment(), ActivityCompat.OnRequestPermissionsResultCallback {

    val frameList = ArrayList<ByteArray>()

    companion object {
        private const val TAG = "Camera2Fragment"
        @JvmStatic
        fun newInstance(): Camera2Fragment = Camera2Fragment()
    }

    // private var dataing: Boolean = false
    private lateinit var textureView: TextureView
    // private lateinit var button: Button

    /**
     * A [Semaphore] to prevent the app from exiting before closing the camera.
     */
    private val cameraOpenCloseLock = Semaphore(1)


    /** A [Handler] for running tasks in the background. */
    private var backgroundHandler: Handler? = null
    private var backgroundThread: HandlerThread? = null

    /** 0为前置摄像头， 1为后置摄像头 */
    private var cameraIds: Array<String?> = arrayOfNulls(2)
    private var currentCameraId: String? = ""

    private var cameraDevice: CameraDevice? = null
    private var imageReader: ImageReader? = null
    private var previewSession: CameraCaptureSession? = null
    private var recordSession: CameraCaptureSession? = null
    private lateinit var previewRequestBuilder: CaptureRequest.Builder
    private lateinit var recordRequestBuilder: CaptureRequest.Builder
    private lateinit var previewRequest: CaptureRequest
    private lateinit var recordRequest: CaptureRequest
    // lateinit var processFrameHandlerThread: ProcessWithHandlerThread
    // lateinit var processFrameHandler: Handler
    val WHAT_PROCESS_FRAME = 1
    lateinit var previewFrame: PreviewFrame
    lateinit var processWithThreadPool: ProcessWithThreadPool

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_camera2, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textureView = capture_preview_view

        //  processFrameHandlerThread = ProcessWithHandlerThread("process frame")
        //  processFrameHandler = Handler(processFrameHandlerThread.getLooper(), processFrameHandlerThread)

        processWithThreadPool = ProcessWithThreadPool()
    }

    override fun onResume() {
        super.onResume()
        if (!requestPermission()) return
        prepareThread()
        detectionCamera()
        calculateCameraParameters()
        if (textureView.isAvailable) {
            openCamera(textureView.width, textureView.height)
            Log.d(TAG, "width :" + textureView.width + " height : " + textureView.height);
        } else {
            textureView.surfaceTextureListener = surfaceTextureListener
        }
    }

    override fun onPause() {
        closeCamera()
        stopThread()
        super.onPause()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.size != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance(getString(R.string.request_permission))
                        .show(childFragmentManager, "request permission camera")
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    /** 请求权限 */
    private fun requestPermission(): Boolean {
        val context = activity
        return if (context == null) {
            false
        } else {
            val permission = ContextCompat.checkSelfPermission(context.applicationContext, Manifest.permission.CAMERA)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    ConfirmationDialog().show(childFragmentManager, "request camera")
                } else {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
                }
                false
            } else {
                true
            }
        }
    }

    /** 准备工作线程 */
    private fun prepareThread() {
        backgroundThread = HandlerThread("CaptureV2").also { it.start() }
        backgroundHandler = Handler(backgroundThread?.looper)
    }

    /** 检测摄像头 */
    private fun detectionCamera() {
        val manager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        for (cameraId in manager.cameraIdList) {
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val cameraDirection = characteristics.get(CameraCharacteristics.LENS_FACING)
            if (cameraDirection == CameraCharacteristics.LENS_FACING_FRONT) {
                cameraIds[0] = cameraId
            } else if (cameraDirection == CameraCharacteristics.LENS_FACING_BACK) {
                cameraIds[1] = cameraId
            }
        }
        currentCameraId = cameraIds[1]
    }

    /** 根据当前摄像头计算所需参数 */
    private fun calculateCameraParameters() {
        val context = activity
        if (context != null) {
            val manager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
            val characteristics: CameraCharacteristics = manager.getCameraCharacteristics(currentCameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            val largest = Collections.min(Arrays.asList(*map.getOutputSizes(ImageFormat.YUV_420_888)), CompareSizesByArea())
            imageReader = ImageReader.newInstance(largest.width, largest.height, ImageFormat.YUV_420_888, /*maxImages*/ 2).apply {
                setOnImageAvailableListener(onImageAvailableListener, backgroundHandler)
            }
        } else {
            Log.d(TAG, "context is null")
        }
    }

    @SuppressLint("MissingPermission")
    private fun openCamera(width: Int, height: Int) {
        val manager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            // Wait for camera to open - 2.5 seconds is sufficient
            if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw RuntimeException("Time out waiting to lock camera opening.")
            }
            manager.openCamera(currentCameraId, deviceStateCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera opening.", e)
        }
    }

    /** 创建camera preview会话  */
//    private fun createCameraPreviewSession() {
//        try {
//            val texture = textureView.surfaceTexture
//            val surface = Surface(texture)
//            previewRequestBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
//            previewRequestBuilder.addTarget(surface)
//            /** camera数据发送到两个surface上 */
//            cameraDevice?.createCaptureSession(Arrays.asList(surface), captureStateCallback, null)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    /** 创建录制会话 */
    private fun createCameraRecordSession() {
        try {
            //closePreviewSession()
            //dataing = true
            val texture = textureView.surfaceTexture
            val surface = Surface(texture)
            recordRequestBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
            recordRequestBuilder.addTarget(surface)
            recordRequestBuilder.addTarget(imageReader?.surface)
            /** camera数据发送到两个surface上 */
            cameraDevice?.createCaptureSession(Arrays.asList(surface, imageReader?.surface), recordSessionStateCallback, null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

//    private fun closePreviewSession() {
//        previewSession?.close()
//        previewSession = null
//    }
//
//    private fun closeRecordSession() {
//        dataing = false
//        createCameraPreviewSession()
//    }

    private fun closeCamera() {
        try {
            cameraOpenCloseLock.acquire()
            previewSession?.close()
            previewSession = null
            cameraDevice?.close()
            cameraDevice = null
            imageReader?.close()
            imageReader = null
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            cameraOpenCloseLock.release()
        }
    }

    private fun stopThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e(TAG, e.toString())
        }
    }

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture?, width: Int, height: Int) {
        }

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture?) = Unit

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture?): Boolean = true

        override fun onSurfaceTextureAvailable(texture: SurfaceTexture?, width: Int, height: Int) {
            openCamera(width, height)
        }
    }

    /** YUV_420_888数据接收 */
    private val onImageAvailableListener = ImageReader.OnImageAvailableListener { reader ->
        // TODO handle YUV_420_888
        backgroundHandler?.post {
            val threadName = Thread.currentThread().name
            val image = reader?.acquireNextImage()
            if (image != null) {

                // 取得 YUV_420_888
                /** Y */
                val Y_plane = image.planes[0]
                val bufferY = Y_plane.buffer
                val bufferYSize = bufferY.remaining()
                val Y_rowStride = Y_plane.getRowStride()
                //Log.d(TAG,"Y_rowStride :"+Y_rowStride)
                // Log.d(TAG,"bufferYSize :"+bufferYSize)

                /** U(Cb) */
                val U_plane = image.planes[1]
                val bufferU = U_plane.buffer
                val bufferUSize = bufferU.remaining()
                val U_rowStride = U_plane.rowStride
                //Log.d(TAG,"U_rowStride :"+U_rowStride)
                //  Log.d(TAG,"bufferUSize :"+bufferUSize)

                /** V(Cr) */
                val V_plane = image.planes[2]
                val bufferV = V_plane.buffer
                val bufferVSize = bufferV.remaining()
                val V_rowStride = U_plane.rowStride
                //Log.d(TAG,"V_rowStride :"+V_rowStride)
                // Log.d(TAG,"bufferVSize :"+bufferVSize)

                /** YUV数据集合
                 *  組出 一個畫面的 YUV 數據
                 *  因為 bufferY, bufferU, bufferV 會持續有數據進來
                 *  需要 用 get 方法 取一個畫面有的 Y,U,V 的數據
                 * */
                val data = ByteArray(bufferYSize + bufferUSize + bufferVSize)
                bufferY.get(data, 0, bufferYSize)
                bufferU.get(data, bufferYSize, bufferUSize)
                bufferV.get(data, bufferYSize + bufferUSize, bufferVSize)

                previewFrame = PreviewFrame(data, Y_rowStride, bufferYSize, bufferUSize, bufferVSize)
                processWithThreadPool.post(previewFrame);

                //previewFrame = PreviewFrame(data,Y_rowStride,bufferYSize , bufferUSize ,bufferVSize )
                //processFrameHandler.obtainMessage(WHAT_PROCESS_FRAME,previewFrame).sendToTarget()

//                Log.d(TAG, "Y plane pixel stride: " + Y_plane.getPixelStride())
//                Log.d(TAG, "U plane pixel stride: " + U_plane.getPixelStride())
//                Log.d(TAG, "V plane pixel stride: " + V_plane.getPixelStride())
//
//                Log.d(TAG, "Y plane length: " + Y_plane.getBuffer().remaining())
//                Log.d(TAG, "U plane length: " + U_plane.getBuffer().remaining())
//                Log.d(TAG, "V plane length: " + V_plane.getBuffer().remaining())

                // Log.d(TAG, "data size = " + data.size + "; $threadName")
                image.close()

            } else {
                Log.d(TAG, "image is null")
            }
        }
    }


    private val deviceStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDevice: CameraDevice) {
            cameraOpenCloseLock.release()
            this@Camera2Fragment.cameraDevice = cameraDevice
            //createCameraPreviewSession()
            createCameraRecordSession()
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            cameraOpenCloseLock.release()
            cameraDevice.close()
            this@Camera2Fragment.cameraDevice = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            onDisconnected(cameraDevice)
            this@Camera2Fragment.activity?.finish()
        }
    }

    private val captureStateCallback = object : CameraCaptureSession.StateCallback() {

        /** camera设置完成，会话创建成功，在此处开始请求 */
        override fun onConfigured(session: CameraCaptureSession?) {
            if (cameraDevice == null) {
                return
            } else {
            }
            previewSession = session
            try {
                /** 自动对焦模式是continuous */
                previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                /** 闪光灯自动模式 */
                previewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH)
                /** 创建请求 */
                previewRequest = previewRequestBuilder.build()
                /** 开始请求 */
                previewSession?.setRepeatingRequest(previewRequest, null, backgroundHandler)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onConfigureFailed(session: CameraCaptureSession?) {
            Log.d(TAG, "onConfigureFailed ===========================")
        }
    }

    private val recordSessionStateCallback = object : CameraCaptureSession.StateCallback() {

        /** camera设置完成，会话创建成功，在此处开始请求 */
        override fun onConfigured(session: CameraCaptureSession?) {
            if (cameraDevice == null) {
                return
            } else {
            }
            recordSession = session
            try {
                /** 自动对焦模式是continuous */
                recordRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                /** 闪光灯自动模式 */
                recordRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH)
                /** 创建请求 */
                recordRequest = recordRequestBuilder.build()
                /** 开始请求 */
                recordSession?.setRepeatingRequest(recordRequest, null, backgroundHandler)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onConfigureFailed(session: CameraCaptureSession?) {
            Log.d(TAG, "onConfigureFailed ===========================")
        }
    }

    class PreviewFrame(frameData:ByteArray,y_rowStride : Int,bufferYSize : Int, bufferUSize : Int,bufferVSize : Int) {
        val data :ByteArray = frameData
        var y_rowStride : Int = y_rowStride
        var bufferYSize : Int = bufferYSize
        var bufferUSize : Int = bufferUSize
        var bufferVSize : Int = bufferVSize
    }


    inner class ProcessWithThreadPool {
        private val workQueue: BlockingQueue<Runnable>
        private val mThreadPool: ThreadPoolExecutor
        val TAG = "ThreadPool"
        val KEEP_ALIVE_TIME: Long = 10
        val TIME_UNIT = TimeUnit.SECONDS

        init {
            val corePoolSize = Runtime.getRuntime().availableProcessors()
            val maximumPoolSize = corePoolSize * 2
            workQueue = LinkedBlockingQueue()
            mThreadPool = ThreadPoolExecutor(corePoolSize, maximumPoolSize, KEEP_ALIVE_TIME, TIME_UNIT, workQueue)
        }

        @Synchronized
        fun post(frameData: PreviewFrame) {
            mThreadPool.execute(Runnable { processFrame(frameData) })
        }

        private fun processFrame(frameData: PreviewFrame) {
            Log.i(TAG, "processFrame")

            var e: Byte = 0xFF_FF_FF_FF.toByte()
            val bufferYSize = frameData.bufferYSize
            val bufferUSize = frameData.bufferUSize
            val bufferVSize = frameData.bufferVSize
            val y_rowStride = frameData.y_rowStride
            val rgbArray: ByteArray = ByteArray(bufferYSize * 3)

//            Log.d(TAG,"bufferYSize :"+bufferYSize)
//            Log.d(TAG,"bufferUSize :"+bufferUSize)
//            Log.d(TAG,"bufferVSize :"+bufferVSize)
//            Log.d(TAG,"y_rowStride :"+y_rowStride)

            for (i in 0..bufferYSize step 2) {

                /**
                 * 基於 yuv420(I420) 的色彩取樣，透過 6x4 的像素點做說明：
                 *   0  1  2  3  4  5
                 *   6  7  8  9 10 11
                 *  12 13 14 15 16 17
                 *  18 19 20 21 22 23
                 *
                 *  在 Android Camera2 能夠分別得到 Y,U,V 通道的資訊
                 *  Y 通道的資訊，透過取樣每個像素點得到，也就是說 0~23 的像素點都會被取樣
                 *  U,V 通道的資訊，會在 4個臨近的像素點，選擇一個像素做一次取樣
                 *  ，可以看成把所有像素分組 成 [0,1,6,7] [2,3,8,9] [4,5,10,11]
                 *  [12,13,18,19] [14,15,20,21] [16,17,22,23] ，每組選一個像素取樣一次，當作該組共用的 U,V 值
                 *  雖然 U,V 取樣的方式相似，但是不一定會對相同的像素作取樣
                 *
                 *  取樣完後 理論上 Y 通道長度 24， U 通道 6 ，V 通道長度 6
                 *  實際上得到的是  Y 通道長度 24， U 通道12 ，V 通道長度 12，因為 UV 通道夾雜空的值
                 *  ，所以實際上會是：(_代表空)
                 *   0  1  2  3  4  5  => Y Y Y Y Y Y
                 *   6  7  8  9 10 11  => Y Y Y Y Y Y
                 *  12 13 14 15 16 17  => Y Y Y Y Y Y
                 *  18 19 20 21 22 23  => Y Y Y Y Y Y
                 *  [0,1,6,7] => U_V_ [2,3,8,9] => U_V_ [4,5,10,11] => U_V_
                 *  [12,13,18,19] => U_V_ [14,15,20,21] => U_V_ [16,17,22,23] => U_V_
                 *
                 *  通道分離排成一維的結果：
                 *  Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y U_U_U_U_U_U_V_V_V_V_V_V_
                 *
                 *  轉換到 RGB888 要以先取的分組的 通道數據，算回 RGB 再依照 Y通道數據的位置 填回去
                 *  if(i+(i/6)*6 <24) {
                 *  val yI1 = i + (i / 6) * 6
                 *  val yI2 = i + (i / 6) * 6 + 1
                 *  val yI3 = i + (i / 6) * 6 + 6
                 *  val yI4 = i + (i / 6) * 6 + 1 + 6
                 *  val uI = i+24
                 *  val vI = i+24+12
                 * }
                 */

                if (i + (i / y_rowStride) * y_rowStride < bufferYSize) {
                    val yI1 = i + (i / y_rowStride) * y_rowStride
                    val yI2 = i + (i / y_rowStride) * y_rowStride + 1
                    val yI3 = i + (i / y_rowStride) * y_rowStride + y_rowStride
                    val yI4 = i + (i / y_rowStride) * y_rowStride + 1 + y_rowStride

                    val uI = i + bufferYSize
                    val vI = i + bufferYSize + bufferUSize

                    // Log.d(TAG, "yI1 : " + yI1 + " yI2 : " + yI2+ " yI3 : " + yI3+" yI4 : " + yI4+" uI : " + uI+ " vI : " + vI)

                    val y1d = unsignedToBytes(frameData.data[yI1])
                    val y2d = unsignedToBytes(frameData.data[yI2])
                    val y3d = unsignedToBytes(frameData.data[yI3])
                    val y4d = unsignedToBytes(frameData.data[yI4])

                    val ud = unsignedToBytes(frameData.data[uI])
                    val vd = unsignedToBytes(frameData.data[vI])

                    // Log.d(TAG, "y1d : " + y1d + " ,y2d : " + y2d+ " ,y3d : " + y3d+" ,y4d : " + y4d+" ,ud : " + ud+ " ,vd : " + vd)

                    yuvToRgb(y1d, ud, vd, rgbArray, yI1)
                    yuvToRgb(y2d, ud, vd, rgbArray, yI2)
                    yuvToRgb(y3d, ud, vd, rgbArray, yI3)
                    yuvToRgb(y4d, ud, vd, rgbArray, yI4)


                }
            }
        }

        /**
         * 轉換成 0 ~ 255
         * 從 Byte 的值介於 -128 ~ 127 換成 Int 0 ~ 255
         */
        fun unsignedToBytes(b: Byte): Int {
            val d: Int = b.toInt() and 255
            return d
        }

        /**
         * 轉換 yuv 到 RGB 色彩空間
         */
        fun yuvToRgb(Y: Int, U: Int, V: Int, rgb: ByteArray, i: Int) {

            val red: Byte = (Y + (360 * (V - 128) shr 8)).toByte()
            val green: Byte = (Y - (88 * (U - 128) + 184 * (V - 128) shr 8)).toByte()
            val blue: Byte = (Y + (455 * (U - 128) shr 8)).toByte()

            Log.d(TAG, "Red : " + red + " Green : " + green + " Blue : " + blue)

            rgb[i] = blue
            rgb[i + 1] = green
            rgb[i + 2] = red


        }
    }
}